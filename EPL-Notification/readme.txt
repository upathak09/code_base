'''
This is a python which gives notification for the next scheduled football match played in the English Premier League.
If the file is kept running till the match starts, all information of the match such as the name of the participating teams, goals scored etc are displayed. This folder has executable code, however one requires to install certain packages listed in the import statements.


Scope for improvement: The file can be added to the daemon process so the part of manually running the script gets eradicated.
'''
