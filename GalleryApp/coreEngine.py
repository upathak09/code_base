from models import *
from utils import *
from TransactionApp.models import Transactions
from GalleryApp.models import *
from CavacServ9.utils import *
import datetime
'''
Function to perform database operations
when user selects attach_bill
operation
'''

def attachbillCoreEng(rscDict):
    rCode, rStr = '0', None
    try:
        attachObj = rscDict['attachObj']
        ref_id = rscDict['ref_id']
        if ref_id == '' and rscDict['status'] == '3':
            attachObj.status =int(rscDict['status'])
        else:
            txnObj = rscDict['txnObj']
            attachObj.status = int(rscDict['status'])
            attachObj.ref_id = rscDict['ref_id']
            txnObj.billUrl = attachObj.attachment_url
            txnObj.save()
        attachObj.save()
    except Exception, e:
        print str(e)
        errorLog(err = ERR_STR_GENERIC_EXCEPTION, exception = str(e))
        rCode, rStr = RESPONSE_CODE_GENERIC_EXCEPTION_ERROR, \
                        RESPONSE_STRING_GENERAL_REPLY
    return rCode, rStr

''' 
This function performs core database operations for creating Happay Gallery Entry;
It takes rscDict{} as input and returns 
response code and string
'''

def createGalleryEntryCoreEng(request):
    rCode, rStr = '0', None
    rCode, rStr = database_entry(get_all_values(request))#create database entry for the current mail and attachments
    print rCode, rStr
    errorLog(err = 'Finished coreEngine!', rc = str(rCode), rs = str(rStr))
    return rCode, rStr

