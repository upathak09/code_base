from django.contrib import admin
from models import *

# Register your models here.

class GalleryAppAdmin (admin.ModelAdmin):
    list_per_page = 1000
    search_fields = ["sender","ref_id","attachementId"]

admin.site.register(Gallery, GalleryAppAdmin)

