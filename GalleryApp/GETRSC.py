from models import *
from config import *
from GalleryApp.models import *
from TransactionApp.models import *
from CavacServ9.utils import *


def displaybilluserGETRSC(rscDict):
    rCode, rStr = '0', None
    sender = rscDict['sender']
    iRscArr = []
    try:
        objArr = Gallery.objects.filter(sender=sender)
        if len(objArr) > 0:
            for i in objArr:
                iRsc = {}
                iRsc['attachment_url'] = i.attachment_url
                iRsc['time_email'] = i.time_email
                iRsc['attachment_name'] = i.attachment_name
                iRsc['ref_id'] = i.ref_id
                iRsc['status'] = i.status
                iRsc['attachmentId'] = i.attachementId
                iRscArr.append(iRsc)
    except Exception, e:
        errorLog(err = ERR_STR_GENERIC_EXCEPTION, exception = str(e))
        rCode, rStr = RESPONSE_CODE_GENERIC_EXCEPTION_ERROR,\
                        RESPONSE_STRING_GENERAL_REPLY
    rscDict['iRscArr'] = iRscArr
    return rCode, rStr, rscDict


def attachbillGETRSC(rscDict):
    rCode,rStr='0',None
    ref_id = rscDict['ref_id']
    try:
        rscDict['attachObj'] = Gallery.objects.get(attachementId = rscDict['attachmentId'])
        if ref_id != '':
            rscDict['txnObj'] = Transactions.objects.get(tId = ref_id)

    except Exception, e:
        errorLog(err = ERR_STR_GENERIC_EXCEPTION, exception = str(e))
        rCode, rStr = RESPONSE_CODE_GENERIC_EXCEPTION_ERROR,\
                        RESPONSE_STRING_GENERAL_REPLY

    return rCode, rStr, rscDict

