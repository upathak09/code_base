# coding: utf-8 -*-
from config import *
from CavacServ9.utils import *


def getNcheckdisplaybilluser(request):
    rCode, rStr = '0', None
    rscDict = {
                            'cid': None,
                            'token':None,
                            'sender':None,
                            'recipient':None,
                            'requestFrom': None,
                            'responseType':'json',
                            'details':{}
    }
    reqdParams = ['cid','token','sender']
    for key,value in rscDict.items():
            if key in request.POST:
                    rscDict[key] = request.POST[key]
                    if key in reqdParams:
                            reqdParams.remove(key)
    if len(reqdParams) != 0:
            rscDict['details']['paramsNotSent'] = ",".join(reqdParams)
            rCode , rStr = RESPONSE_CODE_POSTPARAMS_ERROR,\
                                             RESPONSE_STRING_POST_PARAMS
    return rCode, rStr, rscDict

def getNcheckattachbill(request):
    rCode,rStr='0',None
    rscDict = {
                            'cid': None,
                            'token':None,
                            'attachmentId':None,
                            'ref_id':None,  # transaction Id to which the attachemnt will get attached
                            'status': None,
                            'responseType':None,
                            'requestFrom':None,
                            'details':{}
    }
    reqdParams = ['cid','token','attachmentId','ref_id','status']
    for key,value in rscDict.items():
            if key in request.POST:
                    rscDict[key] = request.POST[key]
                    if key in reqdParams:
                            reqdParams.remove(key)
    if len(reqdParams) != 0:
            rscDict['details']['paramsNotSent'] = ",".join(reqdParams)
            rCode , rStr = RESPONSE_CODE_POSTPARAMS_ERROR,\
                                             RESPONSE_STRING_POST_PARAMS
    return rCode, rStr, rscDict


def getNcheckcreateGalleryEntry(request):
    rCode, rStr = '0', None
    rscDict = {
                            'responseType':'json',
                            'details':{}
    }
    rscDict['sender']=request.POST['sender']
    return rCode, rStr, rscDict

