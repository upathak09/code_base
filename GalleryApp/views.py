from django.views.decorators.csrf import csrf_exempt, csrf_protect, requires_csrf_token, ensure_csrf_cookie
import base64
from django.http import HttpResponse, Http404
import json
from models import *
import datetime
import time
import requests
import uuid
from config import *
from CavacServ9.utils import *
from CavacServ9.config import *
from utils import *
from GETRSC import *
from VLDRSC import *
from requestUtils import *
from coreEngine import *
import HTMLParser
'''
API Structure:
GetNcheck* - checks the incoming request for mandatory parameters as well initialises value
*GETRSC- gets resource from database or other resource
*VLDRSC - validates the resources
*coreEngine - does the core computation part i.e all database insert/update operation
'''

# Api for displaying all bills attached by a particular sender
@csrf_exempt
def displayBillUser(request):
    response = {}
    rCode, rStr, rscDict = getNcheckdisplaybilluser(request)
    if rCode == '0':
        rCode, rStr, rscDict['userId'], rscDict['accAccess'] = authenticateUserV2(rscDict)
        if rCode == '0':
            rCode, rStr,rscDict = displaybilluserGETRSC(rscDict)
            if rCode == '0':
                rscDict['details']['billArr'] = rscDict['iRscArr']
    else:
        pass
    response, responseTypeHeader = returnResponse(rCode, rStr, \
                                                    'displaybilluser', response, \
                                                    rscDict['details'], \
                                                    rscDict['responseType']\
                                                )
    return HttpResponse(response, content_type=responseTypeHeader)


#Api for using an attachment and adding it to a transaction; \
#also provide a status for every attachment in the mail for a
#particular sender

@csrf_exempt
def attachbill(request):
    response = {}
    rCode, rStr, rscDict = getNcheckattachbill(request)
    if rCode == '0':
        rCode, rStr, rscDict['userId'], rscDict['accAccess'] = authenticateUserV2(rscDict)
        if rCode == '0':
            rCode, rStr,rscDict = attachbillGETRSC(rscDict)
            if rCode == '0':
                rCode, rStr,= attachbillVLDRSC(rscDict)
                if rCode == '0':
                    rCode, rStr= attachbillCoreEng(rscDict)

    else:
        pass
    response, responseTypeHeader = returnResponse(rCode, rStr, \
                                                    'attachbill', response, \
                                                    rscDict['details'], \
                                                    rscDict['responseType']\
                                                )
    return HttpResponse(response, content_type=responseTypeHeader)


# Api for creating a gallery of attachments sent by particular sender

@csrf_exempt
def createGalleryEntry(request):
    response = {}
    rCode, rStr, rscDict = getNcheckcreateGalleryEntry(request)
    if rCode == '0':
        rCode, rStr = createGalleryEntryVLDRSC(rscDict)
        if rCode == '0':
            rCode, rStr = createGalleryEntryCoreEng(request)

    errorLog(err = 'Just before sending Response')
    response, responseTypeHeader = returnResponse(rCode, rStr, 'createGalleryEntry', response, {}, rscDict['responseType'])
    return HttpResponse(response, content_type=responseTypeHeader)

