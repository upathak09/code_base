from django.db import models

# Create your models here.
class Gallery(models.Model):
	attachementId = models.CharField(max_length = 255, primary_key = True)#unique id for every attachment
	sender = models.CharField(max_length=255)#sender emailId
	recipient = models.CharField(max_length=255)#recipient emailId
	time_email= models.CharField(max_length=60)#time of email
	attachment_url = models.TextField()#amazon S3 attachment URL
	ref_id = models.TextField(null = True, blank = True)#transaction id where attachment is joined
	attachment_name = models.TextField()#name of attachment
	status = models.IntegerField(default=0)		# 0 = Not attached, 1 = Attached, 2 = Attach n do not show in gallery, 3 = just delete
	created_on = models.DateTimeField(auto_now_add=True)#created time, auto field
 
	def __unicode__(self):#string representation of object
		return  self.attachementId + ' ' + str(self.attachment_url) +' '+ str(self.status)

