from django.conf.urls import patterns, url
from views import *
urlpatterns = patterns('GalleryApp.views',

    url(r'^v3/createentry/$', 'createGalleryEntry'),
    url(r'^v3/displaybilluser/$', 'displayBillUser'),
    url(r'^v3/attachbill/$', 'attachbill'),
)

