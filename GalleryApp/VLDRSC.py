from config import *
from utils import *
from CavacServ9.utils import *
from UserApp.models import UserAccount
from OrganisationApp.models import AccountAccess
import re


def attachbillVLDRSC(rscDict):
    rCode,rStr = '0',None
    attachObj = rscDict['attachObj']
    if attachObj.status == '2':
        rCode, rStr = RESPONSE_CODE_INVALID_ATTACH_STATUS,\
                RESPONSE_STRING_INVALID_ATTACH_STATUS
    if rscDict['status'] not in ['0','1','2','3']:
        rCode, rStr = RESPONSE_CODE_INVALID_STATUS,\
                RESPONSE_STRING_INVALID_STATUS
    return rCode, rStr


def createGalleryEntryVLDRSC(rscDict):
    rCode,rStr = '0', None
    errorLog(err='Inside VLDRSC')
    sender = rscDict['sender']
    flag1 = 0
    flag2 = 0
    try:
        user_obj = UserAccount.objects.get(emailId=sender)
    except Exception as e:
        flag1 = 1

    try:
        org_obj = AccountAccess.objects.get(emailId=sender)
    except Exception as e:
        flag2 = 1

    if flag1 == 1 and flag2 ==1:
        rCode,rStr = RESPONSE_CODE_UNREGISTERED_USER,RESPONSE_STRING_UNREGISTERED_USER
        errorLog(err = 'Validation FAILED in VLDRSC!')
    else:
        errorLog(err = 'Validation Successful in VLDRSC!')

    return rCode, rStr
