from django.shortcuts import render
from CavacServ9.utils import *
from models import *
from config import *
from CavacServ9.config import *
import inspect
from datetime import datetime
import json
import re
import uuid
import requests
import time
from requests.auth import HTTPBasicAuth
from django.conf import settings
from boto.s3.connection import S3Connection
from boto.s3.key import Key
import HTMLParser
import wkhtmltopdf
import pdfkit
import os
from BeautifulSoup import BeautifulSoup
from bs4 import BeautifulSoup as BS
from django.core.mail import EmailMultiAlternatives
from CavacServ9.settings import *
from django.db import transaction
from PricingApp.InvoiceEngine import HtmlToPdf

#WKHTMLTOPDF_PATH = '/usr/local/bin/wkhtmltopdf'


def create_duplicate_name(name_file, name_arr, name_db_arr):
    for i in range(100):
        name_bef_ext, ext= get_name_file(name_file)
        name_file = name_bef_ext + "_" + str(i) + ext
        if name_file not in name_db_arr:
            if name_file not in name_arr:
                break
        else: 
            pass
    return name_file


def checkDuplicateName(name_arr,sender):
    name_obj = list(Gallery.objects.filter(sender = sender, status__in =[0, 1]).values_list('attachment_name', flat = True))
    to_return = []
    for i in name_arr:
        if i not in name_obj and i not in to_return:
            to_return.append(i)
        else:
            to_return.append(create_duplicate_name(i,to_return,name_obj))

    return to_return


'''
Function to read and return file content from a file at 'file_path' in local storage

'''

def read_file(file_path):
    f = open(file_path,'r')
    file_content = f.read()
    f.close()
    return file_content

'''
This function performs actual database insert operation 
'''
def database_gallery(values):
    attach_obj = None
    flag=0
    try:
        attach_obj = Gallery(sender = values['sender'],recipient = values['recipient'],\
            time_email = values['time_email'],\
            attachment_url = values['attachment_url']\
            ,attachment_name = values['attachment_name'],ref_id = values['ref_id']\
            ,status = values['status'],
            attachementId = values['attachmentId'])
        attach_obj.save()
        errorLog(err = 'DB ENTRY SUCCESSFUL!', ex = str(attach_obj))
    except Exception, e:
        errorLog(err = 'NO DB ENTRY', ex = str(e))
        flag=1
    return flag

'''
generate message for successful email sent when attachment is added to the Happay Gallery

'''

def get_success_message():
    text = "Attachment Added successfully to Happay Gallery"
    return text



'''
generate pdf from html content of body_email
and save in local storage and
returns PATH_TO_FILE in local storage

'''

def generate_pdf(filename,file_content):
    try:
        ascii_content = file_content.encode('utf-8','ignore')
        path = PROJECT_PATH + 'BODY_PDF/'
        if not os.path.exists(path):
            os.makedirs(path)
        full_path = PROJECT_PATH +'BODY_PDF/'+filename
        mod_obj = HtmlToPdf()
        output_pdf_path = full_path
        mod_obj.convert_html_to_pdf(input_html_content = ascii_content,output_pdf_path=output_pdf_path)
        #pdfkit.from_string(ascii_content, full_path)
        errorLog(err = '\n Finished Creation of PDF')
        return output_pdf_path
    except Exception, e:
        errorLog(err = '\nerror in generatePDF:-(', exception = str(e))
        full_path = None
    return full_path



'''
Function to get file extension

'''
def get_file_type(name_file,attachFlag):
    substr ="."
    index = name_file.rfind(substr)
    extension = ""
    flag = 0
    if index !=-1:
        index+=1
        for i in range(index,len(name_file)):
            extension += name_file[i]
    valid_extension = ['jpg','jpeg','pdf','txt','doc','png','docx','xls','xlsx']
    if extension.lower() not in valid_extension:
        ext = extension.split("_")[0]
        if ext not in valid_extension:
            if attachFlag == False:
                return extension , 1
            elif attachFlag == True:
                return extension,2
    return extension , flag


def generate_final_url(url):
    common_url = settings.S3_BUCKET
    final_url = common_url + str(url)
    return final_url
'''
This function takes url of mailgun where the attachment is saved in Mailgun's messages API as input and 
the filename of the attachment as input
and returns the parsed attachment file as string
'''

def save_attachment(url,filename):
    try:
        pass1 = 'key-a394c4382c75365fe0af959bfe5c1f9c' #pass-key given to mailgun's domain account
        user = 'api'
        file_to = requests.get(url, auth=HTTPBasicAuth('api', pass1))
        content_type=file_to.headers['content-type']
        file_content=''
        for data in file_to:
            file_content += data
    except Exception as e:
        print str(e)
    return file_content



'''
This function checks for a valid email-id 
'''
def validateEmailId(email):
    pattern = '(^[A-Za-z\d.]+)@[A-Za-z-]+(\.[A-Za-z]+){1,2}$'
    if re.match(pattern, email) or email == '':
        return '0', None
    else:
        return RESPONSE_CODE_INVALID_EMAILID, RESPONSE_STRING_INVALID_EMAILID
'''
This function takes status of attachment as input
and validates for a valid status
'''

def validateStatus(status):
    pattern=re.compile("^[0-9]$")
    status=str(status)
    result=pattern.match(status)
    if result==None:
        return RESPONSE_CODE_INVALID_STATUS,RESPONSE_STRING_INVALID_STATUS
    else:
        return '0',None

def send_failure_message(failed_attachments, sender):
    body_message = "Attachment not accepted in Happay gallery for:\t"
    failed_attachments = [' {0} '.format(elem) for elem in failed_attachments]
    failed_attachments = ''.join(failed_attachments)
    subject, from_email, to = 'Failed Attachments', 'hello@happay.in', sender
    text_content = body_message + str(failed_attachments)
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to], bcc = ['pritam@happay.in','utkarsh@happay.in'])
    msg.send()



def get_name_file(name_file):
    substr="."
    index=name_file.rfind(substr)
    ext = ""
    to_return = ""
    if index != -1:
        to_return = name_file[0:index]
        ext = name_file[index:len(name_file)]
    return to_return, ext



def get_all_values(request):
    content = request.POST
    sender = content.get('sender')
    recipient = content.get('recipient')
    timestamp = content.get('timestamp')
    fmt = "%Y-%m-%d %H:%M:%S"
    time_email = time.strftime(fmt, time.localtime(float(timestamp)))

    list_urls = content.get('attachments')
    attachments_json = {}
    
    #list_urls = eval(list_urls)
    
    if list_urls != None:
        attachments_json = json.loads(list_urls)

    url_all = []
    name_attachment = []
    for key in attachments_json:
        if "url" in key:
            url_all.append(key["url"])
        if "name" in key:
            name_attachment.append(key["name"])

    text_html = content.get('body-html')
    h = HTMLParser.HTMLParser()
    unescaped_body_html = h.unescape(text_html)
    
    b_html = '<html><head><meta charset="utf-8"></head>%s</html>' %(unescaped_body_html)
    to_return = {}
    to_return['sender'] = sender
    to_return['recipient'] = recipient
    to_return['time_email'] = time_email
    to_return['body_html'] = b_html
    to_return['attachment_url'] = url_all
    to_return['name_attachment'] = name_attachment
    errorLog(err = 'get_all_values end', obj = str(to_return))
    return to_return

# add transaction.atomic
#@transaction.atomic
def database_entry(all_values):
    rCode, rStr = '0', None
    spcAttachFlag = map(lambda x: False,all_values['name_attachment'])
    body_html = all_values['body_html']
    body_clean = BeautifulSoup(body_html).text
    check_image_tag = BS(body_html,"html.parser")
    flag_image = check_image_tag.find_all('img')
    if (body_clean  not in ['', None]):
        rCode, rStr,spcAttachFlag = create_body_entry(all_values)
    else:
        if flag_image != []:
            rCode , rStr, spcAttachFlag = create_body_entry(all_values)#image in email body
        else:
            errorLog(err = '\n \n No body part included in email!')
    
    url_all=all_values['attachment_url']
    if url_all:
        if rCode == "0":
            all_values['spcAttachFlag'] = spcAttachFlag
            rCode, rStr, failed_attachments = create_attachment_entry(all_values)
            if rCode == "655": #rCode for invalid extension
                send_failure_message(failed_attachments, all_values['sender'])
    
    return rCode,rStr

def create_body_entry(all_values):
    rCode,rStr = "0", None
    name_body_attachment = str(uuid.uuid1())+"_body.pdf"
    sender = all_values['sender']
    body_html = all_values['body_html']
    attach_url = all_values['attachment_url']
    mod_body_html,spcAttachFlag = check_special_attachment(body_html,all_values['name_attachment'],sender,attach_url)
    if True in spcAttachFlag:#make this an array
        print 'Takes the new value'
        body_html = mod_body_html
        errorLog(err = 'testBodyHTML', body_html = body_html)
    path = "Galleryapp/"+all_values['sender']+"/body_pdfs/"+name_body_attachment
    path_to_file = generate_pdf(name_body_attachment,body_html)
    
    try:
        assert(path_to_file != None), 'Error in generating pdf'
        copyContentsToS3Public(path,path_to_file)#create file in S3 from local storage
        htmlFileName, htmlFileExt = os.path.splitext(path_to_file)
        os.remove(path_to_file)
        htmlFile = htmlFileName+'.html'
        os.remove(htmlFile)
        final_url_body = generate_final_url(path)
        attachmentId = generateTransactionId('HPMAT')
        if path_to_file:
            values_for_db = prepare_db_val(all_values, final_url_body, attachmentId, name_body_attachment)
            flag = push_db(values_for_db)
            try:
                assert(flag == 0), 'DB entry unsuccessful for body_pdf'
            except AssertionError as e:
                errorLog(err = str(e))
    except Exception as e:
        errorLog(err = 'error in create_body_entry', exception = str(e))
        #os.remove(path_to_file)
    return rCode, rStr,spcAttachFlag

def push_db(all_values):
    flag = 0
    try:
        attach_obj = Gallery(sender = all_values['sender'],recipient = all_values['recipient'],\
            time_email = all_values['time_email'],\
            attachment_url = all_values['attachment_url'],\
            attachment_name = all_values['attachment_name'],ref_id = all_values['ref_id'],\
            status = all_values['status'],
            attachementId = all_values['attachmentId'])
        attach_obj.save()
        errorLog(err = 'DB ENTRY SUCCESSFUL!', ex = str(attach_obj))
    except Exception, e:
        errorLog(err = 'NO DB ENTRY', ex = str(e))
        flag = 1
    return flag

def prepare_db_val(all_values, final_url_body, attachmentId, name_body_attachment):
    to_return ={}
    to_return = {'sender' : all_values['sender'],'recipient' : all_values['recipient'],\
                    'time_email' : all_values['time_email'], \
                    'attachment_url' : final_url_body, 'attachmentId' : attachmentId, \
                    'attachment_name' : name_body_attachment,'ref_id' : '','status' : 0}
    return to_return

def create_attachment_entry(all_values):
    rCode, rStr = '0', None
    url_all = all_values['attachment_url']
    sender = all_values['sender']
    spcAttachFlag = all_values['spcAttachFlag']
    name_attachment = all_values['name_attachment']
    name_attachment = checkDuplicateName(name_attachment,sender)
    failed_attachments = []
    for index in range(len(url_all)):
        file_content = save_attachment(url_all[index],name_attachment[index])
        extension , flag_filetype = get_file_type(name_attachment[index],spcAttachFlag[index])
        
        if flag_filetype == 1:
            failed_attachments.append(name_attachment[index])
            continue
        elif flag_filetype == 2:#special attachment present but not to be added in DB
            continue
        filePath = PROJECT_PATH+'BODY_PDF/'+name_attachment[index]
        f = open(filePath,'w')
        f.write(file_content)
        f.close()

        extension =extension.split("_")[0]
        path = "Galleryapp/"+all_values['sender']+"/"+extension+"/"+name_attachment[index]#generate_path function to be added here
        #url = createNReturnS3File(path,file_content)
        copyContentsToS3Public(path, filePath)
        os.remove(filePath)
        final_url = generate_final_url(path)
        attachmentId = generateTransactionId('HPMAT')
        values_for_db = prepare_db_val(all_values, final_url, attachmentId, name_attachment[index])
        flag = database_gallery(values_for_db)
        try:
            assert(flag == 0), 'DB entry unsuccessful for attachment'
        except AssertionError as e:
            errorLog(err = str(e))

    
    if len(failed_attachments) != 0:
        rCode , rStr = RESONSE_CODE_INVALID_EXTENSION,\
                            RESPONSE_STRING_INVALID_EXTENSION
    return rCode , rStr , failed_attachments

def check_special_attachment(body_html,attach_list,sender,attach_url):
    soup = BS(body_html,"html.parser")
    spcFlag = map(lambda x: False,attach_list)
    page_images = [image["src"] for image in soup.findAll("img")]
    spc_images = []
    spc_attach_name = [] 
    attachment_url = []
    for attach in attach_list:
        for src in page_images:
            if str(attach.lower()) in str(src.lower()):
                spc_images.append(src)
                spc_attach_name.append(attach)
                attachment_url.append(attach_url[attach_list.index(attach)])#mailgun urls of attachment
    if not len(spc_images)== 0:
        spc_img_urls = send_special_attach(spc_attach_name,sender,attachment_url)
        if spc_img_urls.count(None) < len(spc_img_urls):#check if all images failed while sending to S3        
            for index,to_replace in enumerate(spc_images):
                for url_to_replace in spc_img_urls:
                    if url_to_replace != None:
                        spcFlag[attach_list.index(spc_attach_name[index])] = True
                    new_body_html = body_html.replace(to_replace,url_to_replace)
                    body_html = new_body_html
    return body_html,spcFlag

def send_special_attach(attach_name,sender,attach_url):
    url = []
    for index,attach in enumerate(attach_name):
        path = "Galleryapp/"+sender+"/"+attach+'.png'
        file_content = save_attachment(attach_url[index],attach)
        try:
            if file_content != None:
                f = open(PROJECT_PATH+'BODY_PDF/'+attach+'.png','w')
                f.write(file_content)
                f.close()
            copyContentsToS3Public(path, PROJECT_PATH+'BODY_PDF/'+attach+'.png')
            url.append(generate_final_url(path))
        except Exception as e:
            url.append(None)
    return url
    


