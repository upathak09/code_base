'''
This is an independent app for creating a gallery of attachments for a user. The complete app folder is listed here. However there are 
certain interactions with other modules which are not in this folder. The purpose of creating a gallery is to join the attachment to another
resource namely 'Transaction' in this module. There are other APIs for displaying the gallery and joining the attachment to a resource.
The way it works is, one can send mail to a particular emailId, we use 'Mailgun', a third party application which acts as a listener on the 
particular configured emailId. Upon receiving an email on the particular emailId, Mailgun forwards the mail contents as a json to a custom API
(also configurable). I parse the json, read all contents and then save the body of the email as pdf,mail attachments(if any) in amazon S3. All such 
attachments saved can then be displayed and joined with some resource (such as 'transaction'), so these images/attachments can become a 'bill'.


Scope for improvement:
Right now, signatures sent in email body are not stripped and the pdf created contains mail forwarded part and signatures. Talon is another 
mailgun library which uses machine learning algorithms to strip quoted text and signatures.   
''' 
